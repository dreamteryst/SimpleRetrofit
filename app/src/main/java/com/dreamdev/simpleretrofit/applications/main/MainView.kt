package com.dreamdev.simpleretrofit.applications.main

import com.dreamdev.simpleretrofit.models.User

interface MainView {
    fun setAdapterData(items: List<User>)
}