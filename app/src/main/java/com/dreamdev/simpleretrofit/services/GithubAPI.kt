package com.dreamdev.simpleretrofit.services

import com.dreamdev.simpleretrofit.models.UserResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubAPI {
    @GET("search/users")
    fun searchUsers(@Query("q") q: String?): Observable<UserResponse>
}